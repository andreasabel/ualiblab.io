---
layout: default
title : UALib.Homomorphisms module (The Agda Universal Algebra Library)
date : 2021-01-12
author: William DeMeo
---

## <a id="homomorphism-types">Homomorphism Types</a>

This chapter presents the [UALib.Homomorphisms][] module of the [Agda Universal Algebra Library][].

<pre class="Agda">
<a id="306" class="Symbol">{-#</a> <a id="310" class="Keyword">OPTIONS</a> <a id="318" class="Pragma">--without-K</a> <a id="330" class="Pragma">--exact-split</a> <a id="344" class="Pragma">--safe</a> <a id="351" class="Symbol">#-}</a>

<a id="356" class="Keyword">module</a> <a id="363" href="UALib.Homomorphisms.html" class="Module">UALib.Homomorphisms</a> <a id="383" class="Keyword">where</a>

<a id="390" class="Keyword">open</a> <a id="395" class="Keyword">import</a> <a id="402" href="UALib.Homomorphisms.Basic.html" class="Module">UALib.Homomorphisms.Basic</a>
<a id="428" class="Keyword">open</a> <a id="433" class="Keyword">import</a> <a id="440" href="UALib.Homomorphisms.Noether.html" class="Module">UALib.Homomorphisms.Noether</a>
<a id="468" class="Keyword">open</a> <a id="473" class="Keyword">import</a> <a id="480" href="UALib.Homomorphisms.Isomorphisms.html" class="Module">UALib.Homomorphisms.Isomorphisms</a>
<a id="513" class="Keyword">open</a> <a id="518" class="Keyword">import</a> <a id="525" href="UALib.Homomorphisms.HomomorphicImages.html" class="Module">UALib.Homomorphisms.HomomorphicImages</a>

</pre>

--------------------------------------

[← UALib.Relations.Congruences](UALib.Relations.Congruences.html)
<span style="float:right;">[UALib.Homomorphisms.Basic →](UALib.Homomorphisms.Basic.html)</span>

{% include UALib.Links.md %}

