---
layout: default
title : UALib.Subalgebras module (The Agda Universal Algebra Library)
date : 2021-01-14
author: William DeMeo
---

## <a id="subalgebra-types">Subalgebra Types</a>

This chapter presents the [UALib.Subalgebras][]  module of the [Agda Universal Algebra Library][].

<pre class="Agda">

<a id="300" class="Symbol">{-#</a> <a id="304" class="Keyword">OPTIONS</a> <a id="312" class="Pragma">--without-K</a> <a id="324" class="Pragma">--exact-split</a> <a id="338" class="Pragma">--safe</a> <a id="345" class="Symbol">#-}</a>

<a id="350" class="Keyword">module</a> <a id="357" href="UALib.Subalgebras.html" class="Module">UALib.Subalgebras</a> <a id="375" class="Keyword">where</a>

<a id="382" class="Keyword">open</a> <a id="387" class="Keyword">import</a> <a id="394" href="UALib.Subalgebras.Subuniverses.html" class="Module">UALib.Subalgebras.Subuniverses</a>
<a id="425" class="Keyword">open</a> <a id="430" class="Keyword">import</a> <a id="437" href="UALib.Subalgebras.Generation.html" class="Module">UALib.Subalgebras.Generation</a>
<a id="466" class="Keyword">open</a> <a id="471" class="Keyword">import</a> <a id="478" href="UALib.Subalgebras.Subalgebras.html" class="Module">UALib.Subalgebras.Subalgebras</a> <a id="508" class="Keyword">public</a>
<a id="515" class="Keyword">open</a> <a id="520" class="Keyword">import</a> <a id="527" href="UALib.Subalgebras.Univalent.html" class="Module">UALib.Subalgebras.Univalent</a>

</pre>

--------------------------------------

[← UALib.Terms.Compatibility](UALib.Terms.Compatibility.html)
<span style="float:right;">[UALib.Subalgebras.Subuniverses →](UALib.Subalgebras.Subuniverses.html)</span>

{% include UALib.Links.md %}
