---
layout: default
title : UALib.Relations module (The Agda Universal Algebra Library)
date : 2021-01-12
author: William DeMeo
---

## <a id="relation-and-quotient-types">Relation and Quotient Types</a>

This chapter presents the [UALib.Relations][] module of the [Agda Universal Algebra Library][].

<pre class="Agda">
<a id="322" class="Symbol">{-#</a> <a id="326" class="Keyword">OPTIONS</a> <a id="334" class="Pragma">--without-K</a> <a id="346" class="Pragma">--exact-split</a> <a id="360" class="Pragma">--safe</a> <a id="367" class="Symbol">#-}</a>
</pre>

<pre class="Agda">
<a id="396" class="Keyword">module</a> <a id="403" href="UALib.Relations.html" class="Module">UALib.Relations</a> <a id="419" class="Keyword">where</a>

<a id="426" class="Keyword">open</a> <a id="431" class="Keyword">import</a> <a id="438" href="UALib.Relations.Unary.html" class="Module">UALib.Relations.Unary</a>
<a id="460" class="Keyword">open</a> <a id="465" class="Keyword">import</a> <a id="472" href="UALib.Relations.Binary.html" class="Module">UALib.Relations.Binary</a>
<a id="495" class="Keyword">open</a> <a id="500" class="Keyword">import</a> <a id="507" href="UALib.Relations.Quotients.html" class="Module">UALib.Relations.Quotients</a>

</pre>

-------------------------------------

[← UALib.Algebras.Lifts](UALib.Algebras.Lifts.html)
<span style="float:right;">[UALib.Relations.Unary →](UALib.Relations.Unary.html)</span>

{% include UALib.Links.md %}
