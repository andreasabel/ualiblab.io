---
layout: default
title : UALib.Terms module (The Agda Universal Algebra Library)
date : 2021-01-14
author: William DeMeo
---

## <a id="types-for-terms">Types for Terms</a>

This chapter presents the [UALib.Terms][] module of the [Agda Universal Algebra Library][].

<pre class="Agda">
<a id="284" class="Symbol">{-#</a> <a id="288" class="Keyword">OPTIONS</a> <a id="296" class="Pragma">--without-K</a> <a id="308" class="Pragma">--exact-split</a> <a id="322" class="Pragma">--safe</a> <a id="329" class="Symbol">#-}</a>

<a id="334" class="Keyword">module</a> <a id="341" href="UALib.Terms.html" class="Module">UALib.Terms</a> <a id="353" class="Keyword">where</a>

<a id="360" class="Keyword">open</a> <a id="365" class="Keyword">import</a> <a id="372" href="UALib.Terms.Basic.html" class="Module">UALib.Terms.Basic</a>
<a id="390" class="Keyword">open</a> <a id="395" class="Keyword">import</a> <a id="402" href="UALib.Terms.Operations.html" class="Module">UALib.Terms.Operations</a>

</pre>

-------------------------------------

[← UALib.Homomorphisms](UALib.Homomorphisms.html)
<span style="float:right;">[UALib.Terms.Basic →](UALib.Terms.Basic.html)</span>

{% include UALib.Links.md %}
