---
layout: default
title : UALib.Terms.Compatibility module (The Agda Universal Algebra Library)
date : 2021-01-14
author: William DeMeo
---

### <a id="term-compatibility">Term Compatibility</a>

This section presents the [UALib.Terms.Compatibility][] module of the [Agda Universal Algebra Library][].

\begin{code}

{-# OPTIONS --without-K --exact-split --safe #-}

open import UALib.Algebras using (Signature; 𝓞; 𝓥)
open import UALib.Prelude.Preliminaries using (global-dfunext)

module UALib.Terms.Compatibility {𝑆 : Signature 𝓞 𝓥}{gfe : global-dfunext} where

open import UALib.Terms.Operations{𝑆 = 𝑆}{gfe} public

\end{code}



#### <a id="homomorphism compatibility">Homomorphism compatibility</a>

Here we prove that every term commutes with every homomorphism.

\begin{code}

comm-hom-term : {𝓤 𝓦 𝓧 : Universe} → funext 𝓥 𝓦
 →              {X : 𝓧 ̇}(𝑨 : Algebra 𝓤 𝑆) (𝑩 : Algebra 𝓦 𝑆)
                (h : hom 𝑨 𝑩) (t : Term X) (a : X → ∣ 𝑨 ∣)
                -----------------------------------------
 →              ∣ h ∣ ((t ̇ 𝑨) a) ≡ (t ̇ 𝑩) (∣ h ∣ ∘ a)

comm-hom-term _ 𝑨 𝑩 h (ℊ x) a = 𝓇ℯ𝒻𝓁

comm-hom-term fe 𝑨 𝑩 h (node f 𝒕) a =
 ∣ h ∣((f ̂ 𝑨) λ i₁ → (𝒕 i₁ ̇ 𝑨) a)    ≡⟨ ∥ h ∥ f ( λ r → (𝒕 r ̇ 𝑨) a ) ⟩
 (f ̂ 𝑩)(λ i₁ →  ∣ h ∣((𝒕 i₁ ̇ 𝑨) a))  ≡⟨ ap (_ ̂ 𝑩)(fe (λ i₁ → comm-hom-term fe 𝑨 𝑩 h (𝒕 i₁) a))⟩
 (f ̂ 𝑩)(λ r → (𝒕 r ̇ 𝑩)(∣ h ∣ ∘ a))    ∎

\end{code}

Here is an intensional version.

\begin{code}

comm-hom-term-intensional : global-dfunext → {𝓤 𝓦 𝓧 : Universe}{X : 𝓧 ̇}
 →                          (𝑨 : Algebra 𝓤 𝑆)(𝑩 : Algebra 𝓦 𝑆)(h : hom 𝑨 𝑩)(t : Term X)
                            -------------------------------------------------------------
 →                          ∣ h ∣ ∘ (t ̇ 𝑨) ≡ (t ̇ 𝑩) ∘ (λ a → ∣ h ∣ ∘ a)

comm-hom-term-intensional gfe 𝑨 𝑩 h (ℊ x) = 𝓇ℯ𝒻𝓁

comm-hom-term-intensional gfe {X = X} 𝑨 𝑩 h (node f 𝒕) = γ
 where
  γ : ∣ h ∣ ∘ (λ a → (f ̂ 𝑨) (λ i → (𝒕 i ̇ 𝑨) a)) ≡ (λ a → (f ̂ 𝑩)(λ i → (𝒕 i ̇ 𝑩) a)) ∘ _∘_ ∣ h ∣
  γ = (λ a → ∣ h ∣ ((f ̂ 𝑨)(λ i → (𝒕 i ̇ 𝑨) a)))     ≡⟨ gfe (λ a → ∥ h ∥ f ( λ r → (𝒕 r ̇ 𝑨) a )) ⟩
      (λ a → (f ̂ 𝑩)(λ i → ∣ h ∣ ((𝒕 i ̇ 𝑨) a)))     ≡⟨ ap (λ - → (λ a → (f ̂ 𝑩)(- a))) ih ⟩
      (λ a → (f ̂ 𝑩)(λ i → (𝒕 i ̇ 𝑩) a)) ∘ _∘_ ∣ h ∣ ∎
   where
    IH : ∀ a i → (∣ h ∣ ∘ (𝒕 i ̇ 𝑨)) a ≡ ((𝒕 i ̇ 𝑩) ∘ _∘_ ∣ h ∣) a
    IH a i = intensionality (comm-hom-term-intensional gfe 𝑨 𝑩 h (𝒕 i)) a

    ih : (λ a → (λ i → ∣ h ∣ ((𝒕 i ̇ 𝑨) a))) ≡ (λ a → (λ i → ((𝒕 i ̇ 𝑩) ∘ _∘_ ∣ h ∣) a))
    ih = gfe λ a → gfe λ i → IH a i

\end{code}




#### <a id="congruence-compatibility">Congruence compatibility</a>

Next we prove that every term is compatible with every congruence. That is, if `t : Term X` and `θ : Con 𝑨`, then `a θ b → t(a) θ t(b)`.

\begin{code}

compatible-term : {𝓤 : Universe}{X : 𝓤 ̇}
                  (𝑨 : Algebra 𝓤 𝑆)(t : Term X)(θ : Con 𝑨)
                  -----------------------------------------
 →                compatible-fun (t ̇ 𝑨) ∣ θ ∣

compatible-term 𝑨 (ℊ x) θ p = p x

compatible-term 𝑨 (node f 𝒕) θ p = snd ∥ θ ∥ f λ x → (compatible-term 𝑨 (𝒕 x) θ) p

compatible-term' : {𝓤 : Universe} {X : 𝓤 ̇}
                   (𝑨 : Algebra 𝓤 𝑆)(t : Term X) (θ : Con 𝑨)
                   ------------------------------------------
 →                 compatible-fun (t ̇ 𝑨) ∣ θ ∣

compatible-term' 𝑨 (ℊ x) θ p = p x
compatible-term' 𝑨 (node f 𝒕) θ p = snd ∥ θ ∥ f λ x → (compatible-term' 𝑨 (𝒕 x) θ) p

\end{code}

--------------------------------------

[← UALib.Terms.Operations](UALib.Terms.Operations.html)
<span style="float:right;">[UALib.Subalgebras →](UALib.Subalgebras.html)</span>

{% include UALib.Links.md %}
