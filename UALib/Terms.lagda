---
layout: default
title : UALib.Terms module (The Agda Universal Algebra Library)
date : 2021-01-14
author: William DeMeo
---

## <a id="types-for-terms">Types for Terms</a>

This chapter presents the [UALib.Terms][] module of the [Agda Universal Algebra Library][].

\begin{code}
{-# OPTIONS --without-K --exact-split --safe #-}

module UALib.Terms where

open import UALib.Terms.Basic
open import UALib.Terms.Operations

\end{code}

-------------------------------------

[← UALib.Homomorphisms](UALib.Homomorphisms.html)
<span style="float:right;">[UALib.Terms.Basic →](UALib.Terms.Basic.html)</span>

{% include UALib.Links.md %}
