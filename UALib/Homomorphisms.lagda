---
layout: default
title : UALib.Homomorphisms module (The Agda Universal Algebra Library)
date : 2021-01-12
author: William DeMeo
---

## <a id="homomorphism-types">Homomorphism Types</a>

This chapter presents the [UALib.Homomorphisms][] module of the [Agda Universal Algebra Library][].

\begin{code}
{-# OPTIONS --without-K --exact-split --safe #-}

module UALib.Homomorphisms where

open import UALib.Homomorphisms.Basic
open import UALib.Homomorphisms.Noether
open import UALib.Homomorphisms.Isomorphisms
open import UALib.Homomorphisms.HomomorphicImages

\end{code}

--------------------------------------

[← UALib.Relations.Congruences](UALib.Relations.Congruences.html)
<span style="float:right;">[UALib.Homomorphisms.Basic →](UALib.Homomorphisms.Basic.html)</span>

{% include UALib.Links.md %}

