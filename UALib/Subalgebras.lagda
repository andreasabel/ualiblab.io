---
layout: default
title : UALib.Subalgebras module (The Agda Universal Algebra Library)
date : 2021-01-14
author: William DeMeo
---

## <a id="subalgebra-types">Subalgebra Types</a>

This chapter presents the [UALib.Subalgebras][]  module of the [Agda Universal Algebra Library][].

\begin{code}

{-# OPTIONS --without-K --exact-split --safe #-}

module UALib.Subalgebras where

open import UALib.Subalgebras.Subuniverses
open import UALib.Subalgebras.Generation
open import UALib.Subalgebras.Subalgebras public
open import UALib.Subalgebras.Univalent

\end{code}

--------------------------------------

[← UALib.Terms.Compatibility](UALib.Terms.Compatibility.html)
<span style="float:right;">[UALib.Subalgebras.Subuniverses →](UALib.Subalgebras.Subuniverses.html)</span>

{% include UALib.Links.md %}
