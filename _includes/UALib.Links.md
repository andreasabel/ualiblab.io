[absolute value]: https://en.wikipedia.org/wiki/Absolute_value
[Agda]: https://wiki.portal.chalmers.se/agda/pmwiki.php
[Agda Language Reference]: https://agda.readthedocs.io/en/v2.6.1/language
[Agda Standard Library]: https://agda.github.io/agda-stdlib/
[Agda Tools]: https://agda.readthedocs.io/en/v2.6.1/tools/
[Agda Tutorial]: https://people.inf.elte.hu/pgj/agda/tutorial/Index.html
[Agda Universal Algebra Library]: https://gitlab.com/ualib/ualib.gitlab.io/
[Agda UALib]: https://gitlab.com/ualib/ualib.gitlab.io/
[Agda User's Manual]: https://agda.readthedocs.io/en/v2.6.1/
[Agda Wiki]: https://wiki.portal.chalmers.se/agda/pmwiki.php
[agda2-mode]: https://agda.readthedocs.io/en/v2.6.0.1/tools/emacs-mode.html
[agda-ualib]: https://gitlab.com/ualib/ualib.gitlab.io
[Algebraic Effects and Handlers]: https://www.cs.uoregon.edu/research/summerschool/summer18/topics.php#Bauer
[Algebras module]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Algebras.lagda
[Andrej Bauer]: http://www.andrej.com/index.html
[Axioms and Computation]: https://leanprover.github.io/theorem_proving_in_lean/axioms_and_computation.html#

[Bill Lampe]: https://math.hawaii.edu/wordpress/people/william/
[Birkhoff module]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Birkhoff.lagda
[Birkhoff HSP Theorem]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Birkhoff.lagda

[Category Theory in Context]: http://www.math.jhu.edu/~eriehl/context.pdf
[categorytheory.gitlab.io]: https://categorytheory.gitlab.io
[Charles University in Prague]: https://cuni.cz/UKEN-1.html
[Clifford Bergman]: https://orion.math.iastate.edu/cbergman/
[Cliff Bergman]: https://orion.math.iastate.edu/cbergman/
[Bergman (2012)]: https://www.amazon.com/gp/product/1439851298/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=1439851298&linkCode=as2&tag=typefunc-20&linkId=440725c9b1e60817d071c1167dff95fa
[Coercions using Type Classes]: https://leanprover.github.io/theorem_proving_in_lean/type_classes.html#coercions-using-type-classes
[Computer Aided Formal Reasoning]: http://www.cs.nott.ac.uk/~psztxa/g53cfr/
[constructive mathematics]: https://ncatlab.org/nlab/show/constructive+mathematics
[Coq]: http://coq.inria.fr

[Department of Algebra]: https://www.mff.cuni.cz/en/ka
[dependent types]: https://en.wikipedia.org/wiki/Dependent_type
[Dependent Types at Work]: http://www.cse.chalmers.se/~peterd/papers/DependentTypesAtWork.pdf
[Dependently Typed Programming in Agda]: http://www.cse.chalmers.se/~ulfn/papers/afp08/tutorial.pdf

[Emacs]: https://www.gnu.org/software/emacs/
[emacs]: https://www.gnu.org/software/emacs/
[Equality Section]: https://leanprover.github.io/logic_and_proof/first_order_logic.html?highlight=equality#equality

[Formalization of Universal Algebra in Agda]: http://www.sciencedirect.com/science/article/pii/S1571066118300768
[function extensionality]: https://ncatlab.org/nlab/show/function+extensionality

[git repository of the Agda UALib]: https://gitlab.com/ualib/ualib.gitlab.io/
[gitlab/ualib]: https://gitlab.com/ualib/ualib.gitlab.io/
[gitlab.com/ualib/ualib.gitlab.io]: https://gitlab.com/ualib/ualib.gitlab.io

[HoTT]: https://homotopytypetheory.org/
[HoTT book]: https://homotopytypetheory.org/book/
[HoTT-UF-in-Agda]: https://www.cs.bham.ac.uk/~mhe/HoTT-UF-in-Agda-Lecture-Notes/HoTT-UF-Agda.html
[HSP Theorem]: https://en.wikipedia.org/wiki/Variety_(universal_algebra)#Birkhoff's_theorem
[Hyeyoung Shin]: https://hyeyoungshin.github.io/
[Implicit Arguments]: https://leanprover.github.io/theorem_proving_in_lean/dependent_type_theory.html#implicit-arguments
[Inductive Types in Lean]: https://leanprover.github.io/theorem_proving_in_lean/inductive_types.html
[inductive types]: https://en.wikipedia.org/wiki/Intuitionistic_type_theory#Inductive_types
[Introduction to Univalent Foundations of Mathematics with Agda]: https://www.cs.bham.ac.uk/~mhe/HoTT-UF-in-Agda-Lecture-Notes/index.html

[JB Nation]: http://www.math.hawaii.edu/~jb/
[Jeremy Avigad]: http://www.andrew.cmu.edu/user/avigad/

[Lean]: https://leanprover.github.io/
[Lean 2]: https://github.com/leanprover/lean2
[Lean 4]: https://github.com/leanprover/lean4
[lean extension]: https://github.com/leanprover/vscode-lean
[Lean github repository]:  https://github.com/leanprover/lean/
[Lean Reference Manual]: https://leanprover.github.io/reference/
[Lean Standard Library]: https://github.com/leanprover/lean
[Lean Tutorial]: https://leanprover.github.io/tutorial/
[Lean Universal Algebra Library]: https://github.com/UniversalAlgebra/lean-ualib/
[LICENSE]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/LICENSE
[Libor Barto]: http://www.karlin.mff.cuni.cz/~barto/
[Logic and Proof]: https://leanprover.github.io/logic_and_proof/

[markdown]: https://daringfireball.net/projects/markdown/
[Martin Escardo]: https://www.cs.bham.ac.uk/~mhe
[Martin Escardo's installation hints]: https://github.com/martinescardo/HoTT-UF-Agda-Lecture-Notes/blob/master/INSTALL.md
[Martin Hötzel Escardo]: https://www.cs.bham.ac.uk/~mhe
[Martin-Löf dependent type theory]: https://ncatlab.org/nlab/show/Martin-L%C3%B6f+dependent+type+theory
[Martin-Löf type theory]: https://ncatlab.org/nlab/show/Martin-L%C3%B6f+dependent+type+theory
[master]: https://gitlab.com/ualib/ualib.gitlab.io/tree/master
[master branch]: https://gitlab.com/ualib/ualib.gitlab.io/tree/master
[Mathlib]: https://github.com/leanprover-community/mathlib/
[Mathlib documentation]: https://leanprover-community.github.io/papers/mathlib-paper.pdf
[McKenzie, McNulty, Taylor (2018)]: https://www.amazon.com/gp/product/1470442957/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=typefunc-20&creative=9325&linkCode=as2&creativeASIN=1470442957&linkId=b3109d9c28ceb872df7d4b84b1cc4f29
[MHE]: https://www.cs.bham.ac.uk/~mhe
[Midlands Graduate School in the Foundations of Computing Science]: http://events.cs.bham.ac.uk/mgs2019/
[Miklós Maróti]: http://www.math.u-szeged.hu/~mmaroti/
[MLTT]: https://ncatlab.org/nlab/show/Martin-L%C3%B6f+dependent+type+theory
[More on Implicit Arguments]: https://leanprover.github.io/theorem_proving_in_lean/interacting_with_lean.html?highlight=implicit#more-on-implicit-arguments

[NuPRL]: http://www.nuprl.org/

[OPLSS 2018]: https://www.cs.uoregon.edu/research/summerschool/summer18/topics.php#Bauer

[Peter Mayr]: http://math.colorado.edu/~mayr/
[Preface module]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Preface.lagda
[Prelude.lagda]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Prelude.lagda
[Prelude module]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Prelude.lagda
[Programming Languages Foundations in Agda]: https://plfa.github.io/
[Programming Language Foundations in Agda]: https://plfa.github.io/
[proof assistant]: https://en.wikipedia.org/wiki/Proof_assistant
[proof tactics]: https://en.wikipedia.org/wiki/Tactic_(proof_assistant)

[Ralph Freese]: https://math.hawaii.edu/~ralph/
[reading material]: https://arxiv.org/abs/1807.05923
[Relations module]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Relations.lagda
[Riehl (2017)]: http://www.math.jhu.edu/~eriehl/context/

[Siva Somayyajula]: http://www.cs.cmu.edu/~ssomayya/
[Subalgebras module]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Subalgebras.lagda

[Terms module]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Terms.lagda
[the main Agda website]: https://wiki.portal.chalmers.se/agda/pmwiki.php
[Theorem Proving in Lean]: https://leanprover.github.io/theorem_proving_in_lean/index.html
[this gist]: https://gist.github.com/andrejbauer/3cc438ab38646516e5e9278fdb22022c
[TPL]: https://leanprover.github.io/theorem_proving_in_lean/
[type theory]: https://en.wikipedia.org/wiki/Type_theory
[Type Theory and Formal Proof]: https://www.cambridge.org/vi/academic/subjects/computer-science/programming-languages-and-applied-logic/type-theory-and-formal-proof-introduction
[Type Topology]: https://github.com/martinescardo/TypeTopology

[ualib]: https://gitlab.com/ualib/ualib.gitlab.io
[UALib]: https://gitlab.com/ualib/ualib.gitlab.io
[ualib/ualib.gitlab.io]: https://gitlab.com/ualib/ualib.gitlab.io

[Course on Univalent Foundations]: https://www.cs.bham.ac.uk/~mhe/HoTT-UF-in-Agda-Lecture-Notes
[Univalent Foundations with Agda]: https://www.cs.bham.ac.uk/~mhe/HoTT-UF-in-Agda-Lecture-Notes
[Univalent Foundations and Homotopy Type Theory]: https://www.cs.bham.ac.uk/~mhe/HoTT-UF-in-Agda-Lecture-Notes

[Varieties module]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Varieties.lagda
[Venanzio Capretta]: https://www.duplavis.com/venanzio/
[vscode]: https://code.visualstudio.com/

[William DeMeo]: https://williamdemeo.gitlab.io/
[williamdemeo@gmail.com]: mailto:williamdemeo@gmail.com
[williamdemeo at gmail dot com]: mailto:williamdemeo@gmail.com
[williamdemeo.org]: https://williamdemeo.gitlab.io/
[williamdemeo@gitlab]: https://gitlab.com/williamdemeo
[williamdemeo@github]: https://github.com/williamdemeo


[Issues]: https://gitlab.com/ualib/ualib.gitlab.io/-/issues
[issue]: https://gitlab.com/ualib/ualib.gitlab.io/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=
[new issue]: https://gitlab.com/ualib/ualib.gitlab.io/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=
[feature request]: https://gitlab.com/ualib/ualib.gitlab.io/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=
[merge requests]: https://gitlab.com/ualib/ualib.gitlab.io/-/merge_requests
[merge request]: https://gitlab.com/ualib/ualib.gitlab.io/-/merge_requests/new



[Preface]: UALib.Preface.html

[Prelude]: UALib.Prelude.html
[Prelude.Preliminaries]: UALib.Prelude.Preliminaries.html
[Prelude.Equality]: UALib.Prelude.Equality.html
[Prelude.Inverses]: UALib.Prelude.Inverses.html
[Prelude.Extensionality]: UALib.Prelude.Extensionality.html
[Prelude.Lifts]: UALib.Prelude.Lifts.html

[Relations]: UALib.Relations.html
[Relations.Unary]: UALib.Relations.Unary.html
[Relations.Binary]: UALib.Relations.Binary.html
[Relations.Quotients]: UALib.Relations.Quotients.html

[Algebras]: UALib.Algebras.html
[Algebras.Signatures]: UALib.Algebras.Signatures.html
[Algebras.Algebras]: UALib.Algebras.Algebras.html
[Algebras.Products]: UALib.Algebras.Products.html
[Algebras.Congruences]: UALib.Algebras.Congruences.html

[Homomorphisms]: UALib.Homomorphisms.html
[Homomorphisms.Basic]: UALib.Homomorphisms.Basic.html
[Homomorphisms.Noether]: UALib.Homomorphisms.Noether.html
[Homomorphisms.Isomorphisms]: UALib.Homomorphisms.Isomorphisms.html
[Homomorphisms.HomomorphicImages]: UALib.Homomorphisms.HomomorphicImages.html

[Terms]: UALib.Terms.html
[Terms.Basic]: UALib.Terms.Basic.html
[Terms.Operations]: UALib.Terms.Operations.html

[Subalgebras]: UALib.Subalgebras.html
[Subalgebras.Subuniverses]: UALib.Subalgebras.Subuniverses.html
[Subalgebras.Generation]: UALib.Subalgebras.Generation.html
[Subalgebras.Subalgebras]: UALib.Subalgebras.Subalgebras.html
[Subalgebras.Univalent]: UALib.Subalgebras.Univalent.html

[Varieties]: UALib.Varieties.html
[Varieties.EquationalLogic]: UALib.Varieties.EquationalLogic.html
[Varieties.Varieties]: UALib.Varieties.Varieties.html
[Varieties section]: UALib.Varieties.Varieties.html
[Varieties.Preservation]: UALib.Varieties.Preservation.html

[Birkhoff]: UALib.Birkhoff.html
[Birkhoff.FreeAlgebra]: UALib.Birkhoff.FreeAlgebra.html
[Birkhoff.HSPTheorem]: UALib.Birkhoff.HSPTheorem.html

[UALib.Preface]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Preface.lagda

[UALib.Prelude]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Prelude.lagda
[UALib.Prelude module]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Prelude.lagda
[UALib.Prelude.Preliminaries]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Prelude/Preliminaries.lagda
[UALib.Prelude.Equality]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Prelude/Equality.lagda
[UALib.Prelude.Inverses]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Prelude/Inverses.lagda
[UALib.Prelude.Extensionality]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Prelude/Extensionality.lagda
[UALib.Prelude.Lifts]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Prelude/Lifts.lagda

[UALib.Algebras]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Algebras.lagda
[UALib.Algebras module]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Algebras.lagda
[UALib.Algebras.Signatures]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Algebras/Signatures.lagda
[UALib.Algebras.Algebras]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Algebras/Algebras.lagda
[UALib.Algebras.Products]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Algebras/Products.lagda
[UALib.Algebras.Congruences]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Algebras/Congruences.lagda

[UALib.Relations module]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Relations.lagda
[UALib.Relations]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Relations.lagda
[UALib.Relations.Unary]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Relations/Unary.lagda
[UALib.Relations.Binary]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Relations/Binary.lagda
[UALib.Relations.Quotients]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Relations/Quotients.lagda

[UALib.Homomorphisms.Basic module]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Homomorphisms/Basic.lagda
[UALib.Homomorphisms.Noether module]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Homomorphisms/Noether.lagda
[UALib.Homomorphisms.Isomorphisms module]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Homomorphisms/Isomorphisms.lagda
[UALib.Homomorphisms.HomomorphicImages module]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Homomorphisms/HomomorphicImages.lagda

[UALib.Homomorphisms module]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Homomorphisms.lagda
[UALib.Homomorphisms]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Homomorphisms.lagda
[UALib.Homomorphisms.Basic]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Homomorphisms/Basic.lagda
[UALib.Homomorphisms.Noether]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Homomorphisms/Noether.lagda
[UALib.Homomorphisms.Isomorphisms]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Homomorphisms/Isomorphisms.lagda
[UALib.Homomorphisms.HomomorphicImages]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Homomorphisms/HomomorphicImages.lagda

[UALib.Terms module]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Terms.lagda
[UALib.Terms]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Terms.lagda
[UALib.Terms.Basic]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Terms/Basic.lagda
[UALib.Terms.Operations]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Terms/Operations.lagda

[UALib.Subalgebras module]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Subalgebras.lagda
[UALib.Subalgebras]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Subalgebras.lagda
[UALib.Subalgebras.Subuniverses]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Subalgebras/Subuniverses.lagda
[UALib.Subalgebras.Generation]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Subalgebras/Generation.lagda
[UALib.Subalgebras.Properties]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Subalgebras/Properties.lagda
[UALib.Subalgebras.Homomorphisms]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Subalgebras/Homomorphisms.lagda
[UALib.Subalgebras.Subalgebras]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Subalgebras/Subalgebras.lagda
[UALib.Subalgebras.Univalent]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Subalgebras/Univalent.lagda

[UALib.Varieties]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Varieties.lagda
[UALib.Varieties module]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Varieties.lagda
[UALib.Varieties.EquationalLogic]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Varieties/EquationalLogic.lagda
[UALib.Varieties.Varieties]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Varieties/Varieties.lagda
[UALib.Varieties.Preservation]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Varieties/Preservation.lagda

[UALib.Birkhoff]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Birkhoff.lagda
[UALib.Birkhoff module]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Birkhoff.lagda
[UALib.Birkhoff.FreeAlgebra]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Birkhoff/FreeAlgebra.lagda
[UALib.Birkhoff.HSPLemmas]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Birkhoff/HSPLemmas.lagda
[UALib.Birkhoff.HSPTheorem]: https://gitlab.com/ualib/ualib.gitlab.io/-/blob/master/UALib/Birkhoff/HSPTheorem.lagda



<!--
[core.lean]: https://github.com/leanprover/lean/blob/master/library/init/core.lean

[free.lean]: https://gitlab.com/ualib/lean-ualib/tree/william/src/free.lean
[function.lean]: https://github.com/leanprover/lean/blob/master/library/init/function.lean
[functions.lean]: https://github.com/leanprover/lean/blob/master/library/init/algebra/functions.lean

[lattice.lean]: https://github.com/leanprover-community/mathlib/blob/master/src/data/set/lattice.lean
[`lean/library/init`]: https://github.com/leanprover/lean/tree/master/library/init
[`lean/library/init/algebra`]: https://github.com/leanprover/lean/blob/master/library/init/algebra
[`lean/library/init/data`]: https://github.com/leanprover/lean/tree/master/library/init/data
[lean_src]: https://github.com/leanprover/lean
[logic.lean]: https://github.com/leanprover/lean/blob/master/library/init/logic.lean

[`mathlib/src/data/set/basic.lean`]: https://github.com/leanprover-community/mathlib/blob/master/src/data/set/basic.lean

[`nat/`]: https://github.com/leanprover/lean/blob/master/library/init/data/nat

[order.lean]: https://github.com/leanprover/lean/blob/master/library/init/algebra/order.lean

[prod.lean]: https://github.com/leanprover/lean/blob/master/library/init/data/prod.lean
[propext.lean]: https://github.com/leanprover/lean/blob/master/library/init/propext.lean

[quot.lean]: https://github.com/leanprover/lean/blob/master/library/init/data/quot.lean
[quotient.lean]: https://gitlab.com/ualib/lean-ualib/blob/william/src/quotient.lean

[setoid.lean]: https://github.com/leanprover/lean/blob/master/library/init/data/setoid.lean
[`sigma/`]: https://github.com/leanprover/lean/blob/master/library/init/data/sigma/
[`sigma/basic.lean`]: https://github.com/leanprover/lean/blob/master/library/init/data/sigma/basic.lean




-->
